from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *

def main(request):
    return render(request, 'pages/1.html')
